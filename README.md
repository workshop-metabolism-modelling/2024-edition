# Workshop Metabolism Modelling - edition 2024 - Web site

Ce site utilise [Nuxt 3](https://nuxt.com/docs/getting-started/introduction).

## Setup

Pour installer les dépendances:

```bash
npm install
```

## Serveur de développement

Pour lancer le serveur de développement

```bash
npm run dev
```

## Production

La mise en production sur [le site](https://workshop-metabolism-modelling.pages.mia.inra.fr/2024-edition) se fait automatiquement via le CI/CD.

## Test de la génération du site statique

Cette étape se fait dans le CI/CD mais vous pouvez quand même tester en local.

```bash
NUXT_APP_BASE_URL="/2024-edition" npm run generate
```

Le site produit sera dans .output/public.
