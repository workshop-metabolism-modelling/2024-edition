import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

import colors from 'vuetify/lib/util/colors'

export default defineNuxtPlugin(nuxtApp => {
  const vuetify = createVuetify({
    // ssr: true,
    components,
    directives,
	theme: {
		themes: {
		  light: {
			dark: false,
			colors: {
			  primary: "#08a4a4", 
			  secondary: colors.green.lighten3, 
			},
		  },
		},
	  }
  })

  nuxtApp.vueApp.use(vuetify)
})