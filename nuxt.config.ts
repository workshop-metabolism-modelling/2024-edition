// https://nuxt.com/docs/api/configuration/nuxt-config
import vuetify from 'vite-plugin-vuetify'

export default defineNuxtConfig({

	// dir: {
	// 	public: 'public2'
	// },
	css: ['vuetify/styles', '@mdi/font/css/materialdesignicons.min.css'],
	build: {
		transpile: ['vuetify'],
	},
	vite: {
		// @ts-ignore
		// curently this will lead to a type error, but hopefully will be fixed soon #justBetaThings
		ssr: {
			noExternal: ['vuetify'], // add the vuetify vite plugin
		},
	},
	// @ts-ignore
	// this adds the vuetify vite plugin
	// also produces type errors in the current beta release
	// hooks: {
	// 	'vite:extendConfig': config => {
	// 		config.plugins?.push(
	// 			vuetify()
	// 		)
	// 	},
	// },
	modules: [
		// @ts-ignore
		// this adds the vuetify vite plugin
		// also produces type errors in the current beta release
		async (options, nuxt) => {
			// @ts-ignore
			nuxt.hooks.hook('vite:extendConfig', config => config.plugins?.push(
				vuetify()
			))
		}
	],
	// vite: {
	// 	css: {
	// 		preprocessorOptions: {
	// 			scss: {
	// 				additionalData: '@use "@/assets/settings.scss" as *;'
	// 			}
	// 		}
	// 	}
	// },
	// nitro: {
	// 	output: { dir: '.output', serverDir: '.output/server', publicDir: 'public' }
	// },
	



})